import tensorflow as tf
from tensorflow import keras
import numpy as np

def build_model():
    inputs  = keras.layers.Input(shape=(5,))
    dense1  = keras.layers.Dense(32, activation='relu')(inputs)
    dense2  = keras.layers.Dense(32, activation='relu')(dense1)
    dense3  = keras.layers.Dense(64, activation='relu')(dense2)
    outputs = keras.layers.Dense(1, activation='sigmoid')(dense3)
    model   = keras.Model(inputs=inputs, outputs=outputs)
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['acc'])
    return model

def train_model(model, history):
    if len(history) < 6:
        return
    x = []
    y = []
    for i in range(5, len(history), 1):
        xx = history[i-5:i]
        yy = history[i]
        x.append(xx)
        y.append(yy)
    model.fit(x=x, y=y, epochs=10, verbose=0)

def main_loop():
    model = build_model()
    print(model.summary())
    i = 0
    history = []
    correct = 0
    while True:
        inp = input('input number\n')
        inp = int(inp)
        if i > 10:
            train_model(model, history)
            prediction = model.predict([history[-5:]])
            p = np.round(np.max(prediction))
            if p == inp:
                correct += 1
            print('predicted: ', p)
            print('correct: {:.2%}'.format(correct/(i-10)))
        history.append(inp)
        i += 1
            
if __name__ == '__main__':
    main_loop()
